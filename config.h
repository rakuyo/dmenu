/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Pragmata Pro Mono:size=18"
};
static const char *prompt      = "";      /* -p  option; prompt to the left of input field */
static const char col_bg[]         = "#1c1c1c";
static const char col_fg[]         = "#fcfaf2";
static const char col_grey[]       = "#828282";
static const char col_blue[]       = "#3a8fb7";
static const char col_white[]      = "#e5e5e5";
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { col_grey, col_bg },
	[SchemeSel]  = { col_white, col_blue},
	[SchemeOut]  = { col_grey, col_bg},
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 10;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static const unsigned int border_width = 3;
